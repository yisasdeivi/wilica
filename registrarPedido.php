<?php
/* 
vista de la opcion registrar pedido la cual hace parte de la interfaz vendedor.
*/
require('/php/cn.php');

session_start();
/*
valida la sesion iniciada.Si esta es del tipo definido en la variable de sesion se procede a visualizar el contenido
de la pagina. Si no, es visiualizado el index. Que en este caso es el login.
*/
if(isset($_SESSION["vendedor"])){?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1,minimum-scale=1">
<title>Registro Cliente</title>
 <link rel="stylesheet" href="css/bootstrap.min.css" >
<link rel="stylesheet" href="css/fontello.css">
<link rel="stylesheet" href="css/estilos.css"> 
</head>
<body>
     <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                    <a href="registrarCliente.php">Cliente</a>
                    <a href="registrarPedido.php">Pedido</a>  
                    <a href="consultaV.php">Administrar</a>    
                    <a href="php/logout.php">Cerrar sesion</a>  
                </nav>
            </div>
        </header>
        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
                
                <h2>sistema de control de ventas</h2>
               
              </div>
            </section>
            
        </main>
        <section id = "infor">
        <form action="#" method="post" class="form-register">
        <h3 class="form__titulo">Registra un pedido</h3>
        <div class ="contenedor-inputs" id="mostrarCliente">
         <a type="button" class="btn btn-primary btn-lg btn-block " data-toggle="modal" data-target="#modalCliente" class="input-100" >Agregar Cliente</a>    
         </div>
         <div class ="contenedor-inputs">
         <a type="button" class="btn btn-primary btn-lg btn-block " data-toggle="modal" data-target="#modalProducto" class="input-100" >Agregar Productos</a>   
        
         </div>           
         <div class ="contenedor-inputs">   
        <textarea id="descripcion" name="comentarios" rows="10" cols="40" placeholder="describe el pedido en esta area" class = "input-100" required></textarea>
        <input type="submit" value="Registrar" class="btn-enviar" <?php echo("onclick='registrapedido(\"".$_SESSION["vendedor"]."\")'"); ?>     
         </div>
       </form>
       </section>
      

      <!-- modal cliente-->
     <div class="modal fade" id="modalCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Agregar Cliente</h4>
         <script >setTimeout("buscarregistro('')",5);</script>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Buscar:</label>
            <input type="text" class="form-control" id="recipient-name" placeholder="Buscar Cliente...." onkeyup="buscarregistro(this.value,1)">
          </div>
          <div class="form-group">
    <label for="exampleSelect2">seleccionar cliente</label>
    <div id="mostrar"></div>
  </div>
        </form>
        <div id="resultado"></div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a type="button" class="btn btn-primary" onclick="mostrarCliente();" data-dismiss="modal">Guardar</a>
      </div>
    </div>
  </div>
<!--termina modal cliente--></div>

<!-- modal Productos-->
<div class="modal fade" id="modalProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Agregar Productos</h4> 

      </div>
      <div class="modal-body">
        <form>          
       <label for="exampleSelect2">Seleccionar Articulos</label>
       <script> setTimeout("buscarproducto('')",5)</script>       
       <div id="mostrarP"></div>
       <div id="resultadoP"></div>      
        </form>
        <form> 
        <div class="array" id="array"></div>        
      </form>  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a type="button" class="btn btn-primary" onclick="mostrarCliente()" data-dismiss="modal">Guardar</a>
      </div>   
      </div> 

    </div>
    
  </div>
</div>



</body>



 <script  src="js/jquery.js"></script>
     <script src="js/buscar.js"></script>
     <script src="js/clienteregistro.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
   <script type="text/javascript" src="js/clienteregistro.js"></script>
</html>
<?php	
}else{
	header("location: index.php");
}
?>