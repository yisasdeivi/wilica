<?php
require('/php/cn.php');

session_start();

if(isset($_SESSION["admin"])){?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Wilica</title>
        <meta charset="UFT-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1,minimum-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/estilos.css">
        
        <script >setTimeout ("buscarajax('','pedido')", 5);</script>
        
    </head>
    
    <body>
        <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                    <a href="registrarProducto.php">Producto</a>
                    <a href="registrarVendedor.php">Vendedor</a>
                    <a href="registrarAdministrativo.php">Administrativo</a>
                      <a href="registrarGrupo.php">Grupo</a>
                    <a href="administrar.php">Administrar</a>
                    <a href="php/logout.php">Cerrar sesion</a>
                </nav>
            </div>
        </header>
        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
               
                <h2>sistema de control de ventas</h2>
           
              </div>
            </section>
            <section >         
                <div class="contenedor">
                     
                     <div class="input-group">
                     <input type="text" class="form-control" placeholder="Buscar Por..." onkeyup="buscarajax(this.value,'pedidoA');">
                      <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Go!</button>
                     </span>
                    </div>
                   <div id="mostrar">
               
                </div>            
        		  </div>
    
    
            </section>            
        </main>
        
    </body>
    <!-- jQuery -->
     <script  src="js/jquery.js"></script>
     <script src="js/buscar.js"></script>
     <script src="js/pedido.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
</html>
<?php	
}else{
	header("location: index.php");
}
?>