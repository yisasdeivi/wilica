<?php
require('/php/cn.php');

session_start();

if(isset($_SESSION["admin"])){?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Administrar</title>
        <meta charset="UFT-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1,minimum-scale=1">
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/estilos.css">
        
        
    </head>
    
    <body>
        <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                    <a href="registrarProducto.php">Producto</a>
                    <a href="registrarVendedor.php">Vendedor</a>
                    <a href="registrarAdministrativo.php">Administrativo</a>
                    <a href="registrarGrupo.php">Grupo</a>
                    <a href="administrar.php">Administrar</a>
                    <a href="php/logout.php">Cerrar sesion</a>  
                </nav>
            </div>
        </header>
        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">

                <h2>sistema de control de ventas</h2>
               
              </div>
            </section>
            
            <section id="info">
                <h4>Selecciona una de las siguientes opciones</h4>
                
                <div class="contenedor">
                      <div class="info-wilica">
                        <a href="consultaAdministrativo.php"><img src="img/administrativos.jpg" alt=""></a>
                        <h4>Administrativos</h4>
                    </div>
                    <div class="info-wilica">
                        <a href="consultaProducto.php"><img src="img/productos.jpg" alt=""></a>
                        <h4>Productos</h4>
                    </div>
                    <div class="info-wilica">
                        <a href="consultaVendedor.php"><img src="img/vendedores.jpg" alt=""></a>
                        <h4>Vendedores</h4>
                     </div>
                     <div class="info-wilica">
                        <a href="consultaCliente.php"><img src="img/clientes.jpg" alt=""></a>
                        <h4>Clientes</h4>
                    </div>
                    <div class="info-wilica">
                        <a href="consultaPedido.php"><img src="img/pedido.jpg" alt=""></a>
                        <h4>Pedidos</h4>
                     </div>
                     
                </div>
                
            </section>
            
        </main>
    </body>
</html>
<?php	
}else{
	header("location: index.php");
}
?>