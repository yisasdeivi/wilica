function validarC() {
	
	var nombre,cedula,correo,fechan,telefono,direccion;
	
	nombre = document.getElementById("nombre").value;
	cedula = document.getElementById("cedula").value;
	correo = document.getElementById("correo").value;
	telefono = document.getElementById("telefono").value;
	expresion = /\w+@\w+\.+[a-z]/;
	direccion = document.getElementById("direccion").value;
	
	if(nombre === "" || cedula === "" || correo === "" || telefono === "" || direccion === ""){
		alert("Todos los campos son obligatorios para registrar un producto");	
        return false;
	}
	
	
	if(nombre.length>50){
		alert("el nombre debe contener como maximo 50 caracteres");
		return false;
		
	}
	if(cedula.length>10){
		alert("la cedula debe contener como maximo 10 caracteres");
		return false;
		
	}
	if(correo.length>50){
		alert("el correo debe contener como maximo 50 caracteres");
		return false;
	}
	
	if(telefono.length>10){
		alert("el telefono debe contener como maximo 10 caracteres");
		return false;
		
	}
	if(direccion.length>50){
		alert("la direccion debe contener como maximo 50 caracteres");
		return false;
		
	}
	
	else if(!expresion.test(correo)){
	alert("el correo no es valido");
	return false;
	
	
	}
	else if(isNaN(cedula) || isNaN(telefono) ){
		alert("la cedula y el telefono deben ser valores numericos");
		return false;
	}
}