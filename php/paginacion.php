<?php

  class paginacion{
        
        public $tamtotal;//tamaño total  de la paginacion
        public $paginas; //tamaño de las paginas
        public $posicion;// posicion del comienzo de una tabla
        public $tabla;


         function paginacion($tamaño,$paginas,$tabla){ 
         $this->paginas=$paginas;
         $this->tamtotal=$tamaño/$paginas; 
         $this->tamtotal=(int)$this->tamtotal; 
         $this->tabla=$tabla;        

        }


        public function consulta($sql,$pos){
        $this->posicion=($pos * $this->paginas)-$this->paginas;
        $sql2=$sql." "."limit ".$this->posicion.",".$this->paginas;
        return $sql2;        
        }

        public function paginas($pos){
        $head="<nav aria-label='...'>
        <ul class='pagination' id='mostrar'>
        <li class='page-item'>
        <a class='page-link' href='#'' tabindex='-1'  onclick='paginacion(1,\"".$this->tabla."\")' aria-label='Previous'>
        <span aria-hidden='true'>&laquo;</span>
        <span class='sr-only'>Previous</span>
        </a>
        </li>";   
        $html=$head; 
        $cont=0; 
        $posi=0;         
         if($pos==1){
           $posi=$pos;
         }else
         if($pos==2){
          $posi=$pos-1;
          }
          else{
            $posi=$pos-2;
          }
           for($i=$posi;$i<=$this->tamtotal;$i++){
            if($cont<5){
              if($pos==$i){
              $html.="<li class='page-item active' value=$i onclick='paginacion(this.value,\"".$this->tabla."\")'><a class='page-link' href='#'>$i</a></li>";
              }else{$html.="<li class='page-item' value=$i onclick='paginacion(this.value,\"".$this->tabla."\")'><a class='page-link' href='#'>$i</a></li>";}
          	
            }
            else{break;}
            $cont++;
          }
        
        $this->posicion=$pos;
        $html.="<a class='page-link' href='#'  aria-label='Next' onclick='paginacion($this->tamtotal,\"".$this->tabla."\")'>
        <span aria-hidden='true'>&raquo;</span>
        <span class='sr-only'>Next</span></a></li></ul></nav>";
          return $html;
      }

      public function paginasp($pos){
        $head="<nav aria-label='...'>
        <ul class='pagination' id='mostrar'>
        <li class='page-item'>
        <a class='page-link' href='#'' tabindex='-1'  onclick='paginacionproducto(1)' aria-label='Previous'>
        <span aria-hidden='true'>&laquo;</span>
        <span class='sr-only'>Previous</span>
        </a>
        </li>";   
        $html=$head; 
        $cont=0; 
        $posi=0;         
         if($pos==1){
           $posi=$pos;
         }else
         if($pos==2){
          $posi=$pos-1;
          }
          else{
            $posi=$pos-2;
          }
           for($i=$posi;$i<=$this->tamtotal;$i++){
            if($cont<5){
              if($pos==$i){
              $html.="<li class='page-item active' value=$i onclick='paginacionproducto($i)'><a class='page-link' href='#'>$i</a></li>";
              }else{$html.="<li class='page-item' value=$i onclick='paginacionproducto($i)'><a class='page-link' href='#'>$i</a></li>";}
            
            }
            else{break;}
            $cont++;
          }
        
        $this->posicion=$pos;
        $html.="<a class='page-link' href='#'  aria-label='Next' onclick='paginacionproducto($this->tamtotal)'>
        <span aria-hidden='true'>&raquo;</span>
        <span class='sr-only'>Next</span></a></li></ul></nav>";
          return $html;
      }

      public function paginasCliente($pos){
        $head="<nav aria-label='...'>
        <ul class='pagination' id='mostrar'>
        <li class='page-item'>
        <a class='page-link' href='#'' tabindex='-1'  onclick='paginacionregistro(1)' aria-label='Previous'>
        <span aria-hidden='true'>&laquo;</span>
        <span class='sr-only'>Previous</span>
        </a>
        </li>";   
        $html=$head; 
        $cont=0; 
        $posi=0;         
         if($pos==1){
           $posi=$pos;
         }else
         if($pos==2){
          $posi=$pos-1;
          }
          else{
            $posi=$pos-2;
          }
           for($i=$posi;$i<=$this->tamtotal;$i++){
            if($cont<5){
              if($pos==$i){
              $html.="<li class='page-item active' value=$i onclick='paginacionregistro($i)'><a class='page-link' href='#'>$i</a></li>";
              }else{$html.="<li class='page-item' value=$i onclick='paginacionregistro($i)'><a class='page-link' href='#'>$i</a></li>";}
            
            }
            else{break;}
            $cont++;
          }
        
        $this->posicion=$pos;
        $html.="<a class='page-link' href='#'  aria-label='Next' onclick='onclick=paginacionregistro($this->tamtotal)'>
        <span aria-hidden='true'>&raquo;</span>
        <span class='sr-only'>Next</span></a></li></ul></nav>";
          return $html;
      }


  }