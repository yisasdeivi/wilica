<?php
/* 
vista de la opcion registrar cliente la cual hace parte de la interfaz vendedor.
*/

require('/php/cn.php');

session_start();
/*
valida la sesion iniciada.Si esta es del tipo definido en la variable de sesion se procede a visualizar el contenido
de la pagina. Si no, es visiualizado el index. Que en este caso es el login.
*/
if(isset($_SESSION["vendedor"])){?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1,minimum-scale=1">
<link rel="stylesheet" href="css/fontello.css">
<link rel="stylesheet" href="css/estilos.css"> 
<title>Registro Cliente</title>
<script src="js/validarC.js"></script>
</head>

<body>
     <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                    <a href="registrarCliente.php">Cliente</a>
                    <a href="registrarPedido.php">Pedido</a>  
                    <a href="consultaV.php">Administrar</a>  
                    <a href="php/logout.php">Cerrar sesion</a>    
                </nav>
            </div>
        </header>
        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
                
                <h2>sistema de control de ventas</h2>
                
              </div>
            </section>
            
        </main>
        <section id = "infor">
        <form action="php/registroCliente.php" method="post" class="form-register" onsubmit ="return validarC();">
        <h3 class="form__titulo">Registra un cliente</h3>
         <div class ="contenedor-inputs">
         
          <input type="text" id = "nombre" name="nombre" placeholder="Nombre" class ="input-100" required>
          <input type="text" id = "cedula" name="cedula" placeholder="Cedula" class ="input-100" required>
          <input type="email" id = "correo" name = "correo" placeholder="Correo" class ="input-100" >
          <input type="date" id = "fechan" name="fechan" placeholder="Fecha nacimiento" class ="input-100"  required>
          <input type="text" id = "telefono" name= "telefono" placeholder="Telefono" class ="input-100" > 
          <input type="text" id = "direccion" name= "direccion" placeholder="Direccion" class ="input-100" > 
          <select name= "contexto" class ="input-100">
                          <option value="personaN">Persona Natural</option>
                          <option value="personaJ">Persona Juridica</option>
                         
                  </select>
          
          <input type="submit" value="Registrar" class="btn-enviar">
           
         </div>
       </form>
       </section>
</body>
</html>
<?php	
}else{
	header("location: index.php");
}
?>