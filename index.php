<?php
require('/php/cn.php');

session_start();

if(isset($_SESSION["administrativo"])){
	
	header("location: administrativo.php");
	
}
else if(isset($_SESSION["admin"]))
{
	
	header("location: /html/admin.php");
	
}else if(isset($_SESSION["vendedor"])){
	
	header("location: /html/vendedor.php");
	
}
?>
<!DOCTYPE html>
<html lang="es">

    <head>
        <title>Login</title>
        <meta charset="UFT-8">
        <meta name ="viewport" content="width=device-width, user-scalable=no,   initial-scale=1, maximum-scale=1, minimum-scale=1">
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/login.css">
        <script src = "js/validarL.js"></script>
        </head>
        <body>
         
         <header>
             <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
             </div>
         </header>
          <form action="php/validarLogin.php" method ="post">
            <h2>Login</h2>
             
             <input type="text" placeholder="&#128273; Usuario" name="usuario" required>
             <input type="password" placeholder="&#128273; password" name="clave" required>
             <select name="tipo" class ="input-100">
                    <option value = "administrador"> &#128682; Administrador</option>  
                    <option value ="administrativo">&#128206;Administrativo</option>
                    <option value = "vendedor">&#128176;Vendedor</option>         
             </select>
             <input type="submit" value="Ingresar">   
             
          </form>  
          
        </body>
</html>