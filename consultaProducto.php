<?php

require('/php/cn.php');
$consulta = "select id,grupo from grupo";
$resultado = $conexion->query($consulta);
session_start();
if(isset($_SESSION["admin"])){
   
    ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Wilica</title>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
       <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/estilos.css">
        
         <script >setTimeout ("buscarajax(' ','producto')", 5);</script>
   
    </head>
    
    <body>
        <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                   <a href="registrarProducto.php">Producto</a>
                    <a href="registrarVendedor.php">Vendedor</a>
                    <a href="registrarAdministrativo.php">Administrativo</a>
                      <a href="registrarGrupo.php">Grupo</a>
                    <a href="administrar.php">Administrar</a>
                    <a href="php/logout.php">Cerrar sesion</a>
                     
                </nav>
            </div>
        </header>


        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
               
                <h2>sistema de control de ventas</h2>
           
              </div>
            </section>            
                </div>
            <form>             
            </form>
            <section >         
                <div class="contenedor">
                     <div class="input-group">
                     <input type="text" class="form-control" placeholder="Buscar Por..." onkeyup="buscarajax(this.value,'producto');">
                      <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Go!</button>
                     </span>
                    </div>
                   <div id="mostrar">
               
                </div>            
          </div>
    
    
            </section>
            
        </main>
        <!-- modal actualizar producto-->
         
         <div class="modal fade" id="modalactualizarP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  		<div class="modal-dialog" role="document">
    	<div class="modal-content">
     	 <div class="modal-header">
       		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Actualizar</h4>
      	</div>
     	 <div class="modal-body">
     	
             <h2>Producto</h2>
                <div class ="form-group">
                    
                     <input type="text" class="form-control"id= "nombre" name="nombre" placeholder="Nombre"  required>
                     <input type="text" class="form-control"id= "cantidad" name= "cantidad" placeholder="Cantidad"  required>
                     <select name= "grupo" class="form-control" size="0">
                          <?php while($arreglo = mysqli_fetch_array($resultado)){?>
                          <option value="<?php echo $arreglo['id']?>"><?php echo $arreglo['grupo']?></option>
                          <?php } ?>
                     </select>
                       <select name= "unidad" class="form-control">
                          <option value="par">par</option>
                          <option value="impar">impar</option>
                         
                     </select>
                     <input type="text" class="form-control" id= "codigo_barra" name= "codigo_barra" placeholder="codigo de barras" required>
                     
                
      
    </div>
      </div>      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        <a type="button" class="btn btn-primary" onclick="actualizarProducto();" data-dismiss="modal">Actualizar</a>
      </div>
    </div>
  </div>
</div>
</div>

    </body>

     <!-- jQuery -->
      <script  src="js/jquery.js"></script>
     <script src="js/buscar.js"></script>
     <script src="js/consultaA.js"></script>
       <script src="js/actualizarA.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
  
    
</html>
<?php   
}else{
    header("location: index.php");
}
?>