<?php
/* 
vista de la opcion registrar vendedor la cual hace parte de la interfaz de el administrador del sistema.
*/
require('/php/cn.php');

session_start();
/*
valida la sesion iniciada.Si esta es del tipo definido en la variable de sesion se procede a visualizar el contenido
de la pagina. Si no, es visiualizado el index. Que en este caso es el login.
*/
if(isset($_SESSION["admin"])){?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1,minimum-scale=1">
<link rel="stylesheet" href="css/fontello.css">
<link rel="stylesheet" href="css/estilos.css">
<title>Registro Vendedor</title>  
<script src="js/validarV.js"></script>
</head>
<body>
      <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                    <a href="registrarProducto.php">Producto</a>
                    <a href="registrarVendedor.php">Vendedor</a>
                    <a href="registrarAdministrativo.php">Administrativo</a>
                    <a href="registrarGrupo.php">Grupo</a>
                    <a href="administrar.php">Administrar</a>
                    <a href="php/logout.php">Cerrar sesion</a> 
                </nav>
            </div>
        </header>
        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
                
                <h2>sistema de control de ventas</h2>
            
              </div>
            </section>
            <section id = "infor">
 
    	<form action="php/registroVendedor.php" method="post" class="form-register" onsubmit="return validarV();">
   	 <h2 class="form__titulo">Registra un vendedor</h2>
    	<div class ="contenedor-inputs">
     	 <input type="text" id ="nombre" name="nombre" placeholder="Nombre" class ="input-100" required>
     	 <input type="text" id ="cedula" name="cedula" placeholder="Cedula" class ="input-100" required>
     	 <input type="text" id ="password" name="password" placeholder="password" class ="input-100" required>
     	 <input type="email" id ="correo" name = "correo" placeholder="Correo" class ="input-100" >
     	 <input type="date" id ="fechan" name="fechan" placeholder="Fecha nacimiento" class ="input-100"  required>
      <input type="text" id ="telefono" name= 		"telefono" placeholder="Telefono" class ="input-100" > 
      <input type="submit" value="Registrar" class="btn-enviar" >
      
    </div>
     </form>  
                
            </section>
            
        </main>
    
    
</body>
</html>
<?php	
}else{
	header("location: index.php");
}
?>