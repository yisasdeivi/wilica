<?php
/*
vista de la opcion consultar administrativo, esta hace parte de la interfaz del administrador del sistema.
*/
require('/php/cn.php');

	
	
$consulta ="select id,nombre from tipo";
$resultado = $conexion->query($consulta);




session_start();
/*
valida la sesion iniciada.Si esta es del tipo definido en la variable de sesion se procede a visualizar el contenido
de la pagina. Si no, es visiualizado el index. Que en este caso es el login.
*/
if(isset($_SESSION["admin"])){
    ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Wilica</title>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/estilos.css">
       
         <script >setTimeout ("buscarajax('','administrativo')", 5);</script>
   
    </head>
    
    <body>
        <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                   <a href="registrarProducto.php">Producto</a>
                    <a href="registrarVendedor.php">Vendedor</a>
                    <a href="registrarAdministrativo.php">Administrativo</a>
                      <a href="registrarGrupo.php">Grupo</a>
                    <a href="administrar.php">Administrar</a>
                    <a href="php/logout.php">Cerrar sesion</a>
                </nav>
            </div>
        </header>


        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
               
                <h2>sistema de control de ventas</h2>
           
              </div>
            </section>            
                </div>
            <form>             
            </form>
            <section >         
                <div class="contenedor">
                     <div class="input-group">
                     <input type="text" class="form-control" placeholder="Buscar Por..." onkeyup="buscarajax(this.value,'administrativo');">
                      <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Go!</button>
                     </span>
                    </div>
                   <div id="mostrar">
               
                </div>            
          </div>
    
    
            </section>
            
        </main>
         <!-- modal actualizar administrativo-->
         
         <div class="modal fade" id="modalactualizarA" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  		<div class="modal-dialog" role="documento">
    	<div class="modal-content">
     	 <div class="modal-header">
       		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Actualizar</h4>
      	</div>
     	 <div class="modal-body">
     	  <h2> Administrativo</h2>
            <div class ="form-group">
				  <input type="text" class="form-control"  id ="nombre" name="nombre" placeholder="Nombre"  required >
                  <input type="text" class="form-control"  id ="cedula" name="cedula" placeholder="Cedula" required>
                  <input type="text" class="form-control" id ="password" name="password" placeholder="password"  required>
                  <input type="email" class="form-control"  id ="correo" name= "correo" placeholder="Correo"   >
                  <input type="date" class="form-control"  id ="fechan" name="fechan" placeholder="Fecha value="" nacimiento"  required>
                  <input type="text"class="form-control"  id ="telefono" name= "telefono" placeholder="Telefono"  >
                  
				
                  <select name= "contexto" size="0" class="form-control"  >
                          <?php while($arreglo = mysqli_fetch_array($resultado)){?>
                          <option value="<?php echo $arreglo['id']?>"><?php echo $arreglo['nombre']?></option>
                          <?php } ?>
                  </select>
      </div>      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        <a type="button" class="btn btn-primary" onclick="actualizarAdministrativo();" data-dismiss="modal">Actualizar</a>
      </div>
    </div>
  </div>
</div>
</div>

    </body>

     <!-- jQuery -->
      <script  src="js/jquery.js"></script>
     <script src="js/buscar.js"></script>
          <script src="js/actualizarA.js"></script>
     <script src="js/consultaA.js"></script>


    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
  
    
</html>
<?php   
}else{
    header("location: index.php");
}
?>