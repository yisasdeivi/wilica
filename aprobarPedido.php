<?php
/*
vista de la opcion aprobar pedido, esta hace parte de la interfaz de administrativo
*/

require('/php/cn.php');
session_start();

/*
valida la sesion iniciada.Si esta es del tipo definido en la variable de sesion se procede a visualizar el contenido
de la pagina. Si no, es visiualizado el index. Que en este caso es el login.
*/

if(isset($_SESSION["administrativo"])){?> 
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Administrativo</title>
        <meta charset="UFT-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1,minimum-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/estilos.css">        
        <script >setTimeout ("buscarajax('','pedido')", 5);</script>
        
    </head>
    
    <body>
        <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                    <a href="aprobarPedido.php">Pedidos</a>
              
                  <a href="php/logout.php">Cerrar sesion</a>
                </nav>
            </div>
        </header>
        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
               
                <h2>sistema de control de ventas</h2>
           
              </div>
            </section>   
            <section >         
                <div class="contenedor">
                     
                     <div class="input-group">
                     <input type="text" class="form-control" placeholder="Buscar Por..." onkeyup="buscarajax(this.value,'pedido');">
                      <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Go!</button>
                     </span>
                    </div>
                   <div id="mostrar">
               
                </div>            
        		  </div>
    
    
            </section>            
            
        </main>
         <!-- modal rechazar-->
     <div class="modal fade" id="modalrechazar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Rechazar</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
         <label for="usr"></label>
         <textarea class="form-control" rows="3" id="textorechazado"></textarea>
        </div>
      </div>      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a type="button" class="btn btn-primary" onclick="rechazar();" data-dismiss="modal">Rechazar</a>
      </div>
    </div>
  </div>
</div>
 <!-- modal Detalle-->
     <div class="modal fade" id="modaldetalles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Detalles</h4>
             
      </div>
      <div class="modal-body">        
      <div id="mostrarDetalle"></div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a type="button" class="btn btn-primary" onclick="" data-dismiss="modal">Guardar</a>
      </div>
    </div>
  </div>
</div>
    </body>
    <!-- jQuery -->
      <script  src="js/jquery.js"></script>
     <script src="js/buscar.js"></script>
     <script src="js/pedido.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
  
    
</html>
<?php	
}else{
	header("location: index.php");
}
?>