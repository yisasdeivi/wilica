<?php

require('/php/cn.php');
session_start();
if(isset($_SESSION["admin"])){
   
    ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Wilica</title>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/estilos.css">
       
         <script >setTimeout ("buscarajax(' ','cliente')", 5);</script>
   
    </head>
    
    <body>
        <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                   <a href="registrarProducto.php">Producto</a>
                    <a href="registrarVendedor.php">Vendedor</a>
                    <a href="registrarAdministrativo.php">Administrativo</a>
                    <a href="registrarGrupo.php">Grupo</a>
                    <a href="administrar.php">Administrar</a>
                    <a href="php/logout.php">Cerrar sesion</a>  
                </nav>
            </div>
        </header>


        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
               
                <h2>sistema de control de ventas</h2>
           
              </div>
            </section>            
                </div>
            <form>             
            </form>
            <section >         
                <div class="contenedor">
                     <div class="input-group">
                     <input type="text" class="form-control" placeholder="Buscar Por..." onkeyup="buscarajax(this.value,'cliente');">
                      <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Go!</button>
                     </span>
                    </div>
                   <div id="mostrar">
               
                </div>            
          </div>
    
    
            </section>
            
        </main>
        
         <!-- modal actualizar cliente-->
         
         <div class="modal fade" id="modalactualizarC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  		<div class="modal-dialog" role="document">
    	<div class="modal-content">
     	 <div class="modal-header">
       		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="color:black;">Actualizar</h4>
      	</div>
     	 <div class="modal-body">
     	   <h3>Cliente</h3>
         <div class ="form-group">
         
          <input type="text" id = "nombre" name="nombre" placeholder="Nombre" class="form-control" required>
          <input type="text" id = "cedula" name="cedula" placeholder="Cedula" class="form-control" required>
          <input type="email" id = "correo" name = "correo" placeholder="Correo" class="form-control" >
          <input type="date" id = "fechan" name="fechan" placeholder="Fecha nacimiento" class="form-control"  required>
          <input type="text" id = "telefono" name= "telefono" placeholder="Telefono" class="form-control"> 
          <input type="text" id = "direccion" name= "direccion" placeholder="Direccion" class="form-control" > 
          <select name= "contexto"  class="form-control">
                          <option value="personaN">Persona Natural</option>
                          <option value="personaJ">Persona Juridica</option>
                         
                  </select>
      </div>      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        <a type="button" class="btn btn-primary" onclick="actualizarCliente();" data-dismiss="modal">Actualizar</a>
      </div>
    </div>
  </div>
</div>
</div>
    </body>

     <!-- jQuery -->
     <script  src="js/jquery.js"></script>
     <script src="js/buscar.js"></script>
     <script src="js/consultaA.js"></script>
     <script src="js/actualizarA.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
  
    
</html>
<?php	
}else{
	header("location: index.php");
}
?>