<?php
/*
vista principal del usuario de tipo vendedor.
*/
require('/php/cn.php');

session_start();
/*
valida la sesion iniciada.Si esta es del tipo definido en la variable de sesion se procede a visualizar el contenido
de la pagina. Si no, es visiualizado el index. Que en este caso es el login.
*/
if(isset($_SESSION["vendedor"])){?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Vendedor</title>
        <meta charset="UFT-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1,minimum-scale=1">
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/estilos.css">
        
    </head>
    
    <body>
        <header>
            <div class="contenedor">
                <h1 class="icon-contactanos">Wilica </h1>
                <input type="checkbox" id="menu-bar">
                <label class="icon-menu" for="menu-bar"></label>
                <nav class="menu">
                    <a href="registrarCliente.php">Cliente</a>
                    <a href="registrarPedido.php">Pedido</a>  
                    <a href="consultaV.php">Administrar</a>  
                    <a href="php/logout.php">Cerrar sesion</a>
                </nav>
            </div>
        </header>
        
        <main>
            <section id="banner">
              <img src="img/wilica.jpg">    
              <div class="contenedor">
             
                <h2>sistema de control de ventas</h2>
         
              </div>
            </section>
            
        </main>
    </body>
    
</html>
<?php	
}else{
	header("location: index.php");
}
?>